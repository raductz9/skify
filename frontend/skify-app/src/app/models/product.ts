import { Guid } from "guid-typescript";

export interface Product {
  id: Guid;
  productName: string;
  image: string;
  price: number;
  productType: number;
}
