import { Guid } from "guid-typescript";

export interface Contact {
  id: Guid;
  name: string;
  email: string;
  phoneNumber: string;
  subject: string;
  message: string;
}
