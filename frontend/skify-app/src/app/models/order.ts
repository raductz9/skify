import { Guid } from "guid-typescript";
import { Product } from "./product";

export interface Order {
  id: Guid;
  products: Product[];
  name: string;
  surname: string;
  email: string;
  phoneNumber: string;
  weight: string;
  height: string;
  size: string;
  remarks: string;
  fromDate: string;
  toDate: string;
}
