import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../services/contact.service'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactName: string;
  contactEmail: string;
  contactPhoneNumber: string;
  contactSubject: string;
  contactMessage: string;

  constructor(private contactService: ContactService) {
    this.contactEmail = "";
   }

  ngOnInit(): void {
  }

  addMessage(
    contactName: string,
    contactEmail: string,
    contactPhoneNumber: string,
    contactSubject: string,
    contactMessage: string,
  ){
    this.contactService.addMessage(
      contactName,
      contactEmail,
      contactPhoneNumber,
      contactSubject,
      contactMessage
    )
  }

  numericOnly(event): boolean {    
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  lat=45.348568;
  lng = 25.545578;
}
