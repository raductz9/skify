import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Category } from '../../models/category';
import { FilterService } from '../../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  categories: Category[];
  @Output() emitSelectedFilter = new EventEmitter<number>();
  constructor(private filterService: FilterService) {}

  ngOnInit(): void {
    this.categories = this.filterService.getCategories();
  }

  selectFilter(categoryId: number) {
    this.emitSelectedFilter.emit(categoryId);
  }
}
