import { Component, OnInit } from '@angular/core';
import { Product } from 'app/models/product';
import { OrderService } from 'app/services/order.service';
import { ShoppingCartService } from 'app/services/shopping-cart.service';
import { Guid } from 'guid-typescript';
import * as moment from 'moment';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  daysDifference: number;
  products: Product[];
  orderName:string;
  orderSurname:string;
  orderEmail:string;
  orderPhoneNumber:string;
  orderWeight: string;
  orderHeight: string;
  orderSize: string;
  orderFromDate: string;
  orderToDate: string;
  constructor(private shoppingCartService: ShoppingCartService, private orderService: OrderService) {
    this.orderEmail = "";
   }

  ngOnInit(): void {
    this.shoppingCartService
      .getShoppingCartProducts()
      .subscribe((products) => (this.products = products));
  }

  createOrder(
    products: Product[],
    orderName:string,
    orderSurname:string,
    orderEmail:string,
    orderPhoneNumber:string,
    orderWeight: string,
    orderHeight: string,
    orderSize: string,
    orderRemarks: string,
    orderFromDate: string,
    orderToDate: string
  ){
    this.orderService.createOrder(
      products,
      orderName,
      orderSurname,
      orderEmail,
      orderPhoneNumber,
      orderWeight,
      orderHeight,
      orderSize,
      orderRemarks,
      orderFromDate,
      orderToDate
    )
  }

  deleteProduct(id: Guid)
  {
    this.shoppingCartService.deleteProductFromShoppingCart(id).subscribe();
  }

  deleteAllProducts()
  {
    this.shoppingCartService.deleteAllFromShoppingCart().subscribe();
  }

  removeOnFront(index: number)
  {
    this.products.splice(index, 1);
  }

  numericOnly(event): boolean {    
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  getTotalPricePerDay()
  {
    var total=0;
    this.products.forEach(element => {
      total += element.price
    });
    return total;
  }

  getTotalPrice()
  {
    if(this.daysDifference > 3)
    {
      return this.getTotalPricePerDay()* this.daysDifference - (20/100 * this.getTotalPricePerDay() * this.daysDifference);
    }
    else{
      return this.getTotalPricePerDay()* this.daysDifference;
    }
  }

  numberOfDays(date1: string, date2: string)
  {
    var firstDate = moment(date1);
    var secondDate = moment(date2);
    var numberOfDays = Math.abs(firstDate.diff(secondDate, 'days'));

    this.daysDifference = numberOfDays;
  }
}
