import { Component, Input, OnInit } from '@angular/core';
import { ShoppingCartService } from 'app/services/shopping-cart.service';
import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  constructor(private productService: ProductService, private shoppingCartService: ShoppingCartService) {}
  @Input() selectedCategoryId: number;
  products: Product[];
  name:string;
  image:string;
  price:number;

  ngOnInit(): void {
    this.productService
      .getProducts()
      .subscribe((products) => (this.products = products));
  }

  removeOnFront(index: number) {
    this.products.splice(index, 1);
  }

  saveProductInfo(
    productName:string,
    productImage:string,
    productPrice:number
  )
  {
    this.name = productName;
    this.image = productImage;
    this.price = productPrice;
  }

  addProductInShoppingCart(
      product: Product
      ) {
        this.shoppingCartService.addProductToShoppingCart(
         product
        );
      }

  ngOnChanges(): void {
    if (this.selectedCategoryId) {
      this.productService
        .getFiltredProducts(this.selectedCategoryId)
        .subscribe((filteredProducts) => (this.products = filteredProducts));
    }
  }
}
