import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FinalModule } from './finalModule/final.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [FinalModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
