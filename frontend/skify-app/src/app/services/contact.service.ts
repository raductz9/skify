import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  contextId: Guid;
  readonly baseUrl = 'https://localhost:44348/api';

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  addMessage(
    contactName: string,
    contactEmail: string,
    contactPhoneNumber: string,
    contactSubject: string,
    contactMessage: string
  ) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    let contact = {
      id: this.contextId,
      name: contactName,
      email: contactEmail,
      phoneNumber: contactPhoneNumber,
      subject: contactSubject,
      message: contactMessage,
    };
    return this.httpClient
      .post(this.baseUrl + '/Contact/CreateMessage', contact, {
        headers,
        responseType: 'text',
      })
      .subscribe();
  }
}
