import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root',
})
export class FilterService {
  categories: Category[] = [
    { name: 'TOATE', id: 4 },
    { name: 'SKI', id: 1 },
    { name: 'SNOWBOARD', id: 2 },
    { name: 'ECHIPAMENT', id: 3 }
  ];
  constructor(private category: HttpClient) {}

  getCategories() {
    return this.categories;
  }
}
