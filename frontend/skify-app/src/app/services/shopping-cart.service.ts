import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartService {
  contextId: Guid;
  readonly baseUrl = 'https://localhost:44348/api';

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  getShoppingCartProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(
      this.baseUrl + `/ShoppingCart/GetAllFromShoppingCart`,
      this.httpOptions
    );
  }

  addProductToShoppingCart(
    product: Product
  ) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    product.id = this.contextId;
    return this.httpClient
      .post(this.baseUrl + '/ShoppingCart/CreateProductInShoppingCart', product, {
        headers,
        responseType: 'text',
      })
      .subscribe();
  }

  deleteProductFromShoppingCart(id: Guid) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.httpClient.delete<any>(
      this.baseUrl + `/ShoppingCart/DeleteProductFromShoppingCart?id=`+id,
      options
    );
  }

  deleteAllFromShoppingCart() {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.httpClient.delete<any>(
      this.baseUrl + `/ShoppingCart/DeleteAllFromShoppingCart`,
      options
    );
  }

  getShoppingCartCount(): Observable<number> {
    return this.httpClient.get<number>(
      this.baseUrl + `/ShoppingCart/GetCount`,
      this.httpOptions
    );
  }
}
