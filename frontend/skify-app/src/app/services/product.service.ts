import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  readonly baseUrl = 'https://localhost:44348/api';

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  getFiltredProducts(categoryId: number): Observable<Product[]> {
    return this.httpClient.get<Product[]>(
      this.baseUrl + `/Product/${categoryId}`,
      this.httpOptions
      );
  }

  getProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(
      this.baseUrl + `/Product/GetAllProducts`,
      this.httpOptions
    );
  }
}
