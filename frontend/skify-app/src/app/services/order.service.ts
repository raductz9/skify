import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  readonly baseUrl = 'https://localhost:44348/api';
  contextId: Guid;
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  createOrder(
        orderProducts:Product[],
        orderName:string,
        orderSurname:string,
        orderEmail:string,
        orderPhoneNumber:string,
        orderWeight: string,
        orderHeight: string,
        orderSize: string,
        orderRemarks: string,
        orderFromDate: string,
        orderToDate: string
      ) {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
    
        let order = {
          id: this.contextId,
          products: orderProducts,
          name: orderName,
          surname: orderSurname,
          email: orderEmail,
          phoneNumber: orderPhoneNumber,
          weight: orderWeight,
          height: orderHeight,
          size: orderSize,
          remarks: orderRemarks,
          fromDate: orderFromDate,
          toDate: orderToDate
        };
        return this.httpClient
          .post(this.baseUrl + '/Order/CreateOrder', order, {
            headers,
            responseType: 'text',
          })
          .subscribe();
  }
  
}
