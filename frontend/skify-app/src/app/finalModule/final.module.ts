import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from '../app.component';
import { HomeComponent } from '../components/home/home.component';
import { AppRoutingModule } from '../app-routing.module';
import { ProductService } from '../services/product.service';
import { FilterService } from '../services/filter.service';
import { FilterComponent } from '../components/filter/filter.component';
import { HeaderComponent } from '../components/header/header.component';
import { FooterComponent } from 'app/components/footer/footer.component';
import { AboutusComponent } from 'app/components/aboutus/aboutus.component';
import { ContactComponent } from 'app/components/contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { ProductComponent } from 'app/components/product/product.component';
import { ContactService } from 'app/services/contact.service';
import { ShoppingCartService } from 'app/services/shopping-cart.service';
import { ShoppingCartComponent } from 'app/components/shopping-cart/shopping-cart.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { OrderService } from 'app/services/order.service';
import { EmailValidatorDirective } from 'app/directives/email-validator.directive';


const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'about-us', component: AboutusComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'shopping-cart', component: ShoppingCartComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FilterComponent,
    ProductComponent,
    HeaderComponent,
    FooterComponent,
    AboutusComponent,
    ContactComponent,
    ShoppingCartComponent,
    EmailValidatorDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDatepickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyApILx4xSmAojgtKHvaTK9oMwvytwNUdJw'
    })
  ],
  providers: [ProductService, FilterService, ContactService, ShoppingCartService, OrderService],
  bootstrap: [AppComponent]
})
export class FinalModule {}
