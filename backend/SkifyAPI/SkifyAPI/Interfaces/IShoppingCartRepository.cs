using SkifyAPI.DBModels;
using System;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IShoppingCartRepository
    {
        public void ReadProducts(ref IList<Product> products);
        public void WriteProducts(ref IList<Product> products);
        public IList<Product> GetAll();
        public void Create(Product product);
        public void Delete(Guid id);
        public void DeleteAll();
        public int GetCount();
    }
}
