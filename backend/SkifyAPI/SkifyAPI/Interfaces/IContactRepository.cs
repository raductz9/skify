using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IContactRepository
    {
        public void ReadMessages(ref IList<Contact> messages);
        public void WriteMessages(ref IList<Contact> messages);
        public IList<Contact> GetAll();
        public void Create(Contact message);
    }
}
