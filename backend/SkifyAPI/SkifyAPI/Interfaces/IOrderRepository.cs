using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IOrderRepository
    {
        public void ReadOrders(ref IList<Order> orders);
        public void WriteOrders(ref IList<Order> orders);
        public IList<Order> GetAll();
        public void Create(Order order);
    }
}
