using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IContactService
    {
        public IList<Contact> GetAll();
        public void Create(Contact message);
    }
}
