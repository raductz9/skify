using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IOrderService
    {
        public IList<Order> GetAll();
        public void Create(Order order);
    }
}
