﻿using SkifyAPI.DBModels;
using System.Collections.Generic;
using System;

namespace SkifyAPI.Interfaces
{
    public interface IProductRepository
    {
        public IList<Product> GetAll();
        public IList<Product> GetProductsByType(int productType);
        public Product GetById(Guid id);
        public void Create(Product product);
        public void Update(Product product);
        public void Delete(Guid id);
    }
}
