using SkifyAPI.DBModels;
using System;
using System.Collections.Generic;

namespace SkifyAPI.Interfaces
{
    public interface IShoppingCartService
    {
        public IList<Product> GetAll();
        public void Create(Product product);
        public void Delete(Guid id);
        public void DeleteAll();
        public int GetCount();
    }
}
