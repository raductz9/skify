﻿using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SkifyAPI.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private SkifyDBContext _db;
        public ProductRepository(SkifyDBContext db)
        {
            _db = db;
        }

        public IList<Product> GetAll()
        {
            return _db.Products.ToList();
        }

        public IList<Product> GetProductsByType(int productType)
        {
            if(productType == 4)
            {
                return _db.Products.ToList();
            }
            return _db.Products.Where(option => option.ProductType == productType).ToList();
        }

        public Product GetById(Guid id)
        {
            Product product = _db.Products.Find(id);
            return product;
        }

        public void Create(Product product)
        {
            product.Id = Guid.NewGuid();
            _db.Products.Add(product);
            _db.SaveChanges();
        }

        public void Update(Product product)
        {
            Product update = _db.Products.Find(product.Id);
            if (update != null)
            {
                update.ProductName = product.ProductName;
                update.Image = product.Image;
                update.Price = product.Price;
                update.ProductType = product.ProductType;
                _db.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Product delete = _db.Products.Find(id);
            if (delete != null)
            {
                _db.Products.Remove(delete);
                _db.SaveChanges();
            }
        }
    }
}
