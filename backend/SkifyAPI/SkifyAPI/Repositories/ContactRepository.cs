using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace SkifyAPI.Repositories
{
    public class ContactRepository : IContactRepository
    {
        IList<Contact> messages = new List<Contact>();

        public void ReadMessages(ref IList<Contact> messages)
        {
            string database = System.IO.File.ReadAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-messages.json");
            messages = JsonConvert.DeserializeObject<List<Contact>>(database);
        }

        public void WriteMessages(ref IList<Contact> messages)
        {
            string json = JsonConvert.SerializeObject(messages, Formatting.Indented);
            System.IO.File.WriteAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-messages.json", json);
        }

        public IList<Contact> GetAll()
        {
            ReadMessages(ref messages);
            return messages;
        }

        public void Create(Contact message)
        {
            ReadMessages(ref messages);
            message.Id = Guid.NewGuid();
            messages.Add(message);
            WriteMessages(ref messages);
        }
    }
}
