using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace SkifyAPI.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        IList<Order> orders = new List<Order>();

        public void ReadOrders(ref IList<Order> orders)
        {
            string database = System.IO.File.ReadAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-orders.json");
            orders = JsonConvert.DeserializeObject<List<Order>>(database);
        }

        public void WriteOrders(ref IList<Order> orders)
        {
            string json = JsonConvert.SerializeObject(orders, Formatting.Indented);
            System.IO.File.WriteAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-orders.json", json);
        }

        public IList<Order> GetAll()
        {
            ReadOrders(ref orders);
            return orders;
        }

        public void Create(Order order)
        {
            ReadOrders(ref orders);
            order.Id = Guid.NewGuid();
            orders.Add(order);
            WriteOrders(ref orders);
        }
    }
}
