using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SkifyAPI.Repositories
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        IList<Product> products = new List<Product>();

        public void ReadProducts(ref IList<Product> products)
        {
            string database = System.IO.File.ReadAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-shoppingcart.json");
            products = JsonConvert.DeserializeObject<List<Product>>(database);
        }

        public void WriteProducts(ref IList<Product> products)
        {
            string json = JsonConvert.SerializeObject(products, Formatting.Indented);
            System.IO.File.WriteAllText(@"C:\Users\RCretu\Desktop\dev-skify\skify\database-shoppingcart.json", json);
        }

        public IList<Product> GetAll()
        {
            ReadProducts(ref products);
            return products;
        }

        public void Create(Product product)
        {
            ReadProducts(ref products);
            product.Id = Guid.NewGuid();
            products.Add(product);
            WriteProducts(ref products);
        }

        public void Delete(Guid id)
        {     
            ReadProducts(ref products);
            Product delete = products.FirstOrDefault(o => o.Id == id);
            if (delete != null)
            {
                products.Remove(delete);
            }
            WriteProducts(ref products);
        }

        public void DeleteAll()
        {
            products.Clear();
            WriteProducts(ref products);
        }

        public int GetCount()
        {
            ReadProducts(ref products);
            return products.Count();
        }
    }
}
