using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using System.Collections.Generic;
using System;

namespace SkifyAPI.Services
{
    public class ShoppingCartService : IShoppingCartService
    {
        private readonly IShoppingCartRepository shoppingCartRepository;
        public ShoppingCartService(IShoppingCartRepository shoppingCartRepository)
        {
            this.shoppingCartRepository = shoppingCartRepository;
        }

        public IList<Product> GetAll()
        {
            return shoppingCartRepository.GetAll();
        }

        public void Create(Product product)
        {
            shoppingCartRepository.Create(product);
        }

        public void Delete(Guid id)
        {
            shoppingCartRepository.Delete(id);
        }

        public void DeleteAll()
        {
            shoppingCartRepository.DeleteAll();
        }

        public int GetCount()
        {
            return shoppingCartRepository.GetCount();
        }
    }
}
