﻿using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using System.Collections.Generic;
using System;

namespace SkifyAPI.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;
        public ProductService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public IList<Product> GetAll()
        {
            return productRepository.GetAll();
        }

        public IList<Product> GetProductsByType(int productType)
        {
            return productRepository.GetProductsByType(productType);
        }

        public Product GetById(Guid id)
        {
            return productRepository.GetById(id);
        }

        public void Create(Product product)
        {
            productRepository.Create(product);
        }

        public void Update(Product product)
        {
            productRepository.Update(product);
        }

        public void Delete(Guid id)
        {
            productRepository.Delete(id);
        }
    }
}
