using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository contactRepository;
        public ContactService(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public IList<Contact> GetAll()
        {
            return contactRepository.GetAll();
        }

        public void Create(Contact message)
        {
            contactRepository.Create(message);
        }
    }
}
