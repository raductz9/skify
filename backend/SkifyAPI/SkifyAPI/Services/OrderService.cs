using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using System.Collections.Generic;

namespace SkifyAPI.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public IList<Order> GetAll()
        {
            return orderRepository.GetAll();
        }

        public void Create(Order order)
        {
            orderRepository.Create(order);
        }
    }
}
