﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SkifyAPI.DBModels
{
    public partial class Product
    {
        [Key]
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public string Image { get; set; }
        public int? Price { get; set; }
        public int? ProductType { get; set; }
    }
}
