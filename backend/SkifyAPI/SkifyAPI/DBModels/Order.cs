using System;
using System.Collections.Generic;

namespace SkifyAPI.DBModels
{
    public class Order
    {
        public Guid Id { get; set; }
        public IList<Product> Products { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Remarks { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}
