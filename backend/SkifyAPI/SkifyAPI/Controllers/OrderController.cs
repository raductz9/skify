using System.Collections.Generic;
using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Microsoft.AspNetCore.Mvc;

namespace SkifyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpGet]
        [Route("GetAllOrders")]
        public IEnumerable<Order> GetAllOrders()
        {
            return orderService.GetAll();
        }

        [HttpPost]
        [Route("CreateOrder")]
        public void CreateOrder([FromBody] Order order)
        {
            orderService.Create(order);
        }
    }
}
