﻿using System.Collections.Generic;
using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace SkifyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        [Route("GetAllProducts")]
        public IEnumerable<Product> GetAllProducts()
        {
            return productService.GetAll();
        }

        [HttpGet("{productCategory}")]
        public IEnumerable<Product> GetProductsByType(int productCategory)
        {
            return productService.GetProductsByType(productCategory);
        }

        [HttpGet]
        [Route("GetProductById")]
        public Product GetProductById(Guid id)
        {
            return productService.GetById(id);
        }

        [HttpPost]
        [Route("CreateProduct")]
        public void CreateProduct([FromBody] Product product)
        {
            productService.Create(product);
        }

        [HttpPut]
        [Route("UpdateProduct")]
        public void UpdateProduct([FromBody] Product product)
        {
            productService.Update(product);
        }

        [HttpDelete]
        [Route("DeleteProduct")]
        public void DeleteProduct([FromBody] Guid id)
        {
            productService.Delete(id);
        }
    }
}
