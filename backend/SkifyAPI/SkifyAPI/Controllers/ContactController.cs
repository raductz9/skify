using System.Collections.Generic;
using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Microsoft.AspNetCore.Mvc;

namespace SkifyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService contactService;

        public ContactController(IContactService contactService)
        {
            this.contactService = contactService;
        }

        [HttpGet]
        [Route("GetAllMessages")]
        public IEnumerable<Contact> GetAllMessages()
        {
            return contactService.GetAll();
        }

        [HttpPost]
        [Route("CreateMessage")]
        public void CreateMessage([FromBody] Contact message)
        {
            contactService.Create(message);
        }
    }
}
