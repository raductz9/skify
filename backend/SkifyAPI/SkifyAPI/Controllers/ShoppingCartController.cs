using System.Collections.Generic;
using SkifyAPI.Interfaces;
using SkifyAPI.DBModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace SkifyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IShoppingCartService shoppingCartService;

        public ShoppingCartController(IShoppingCartService shoppingCartService)
        {
            this.shoppingCartService = shoppingCartService;
        }

        [HttpGet]
        [Route("GetAllFromShoppingCart")]
        public IEnumerable<Product> GetAllFromShoppingCart()
        {
            return shoppingCartService.GetAll();
        }

        [HttpPost]
        [Route("CreateProductInShoppingCart")]
        public void CreateProduct([FromBody] Product product)
        {
            shoppingCartService.Create(product);
        }

        [HttpDelete]
        [Route("DeleteProductFromShoppingCart")]
        public void DeleteProduct(Guid id)
        {
            shoppingCartService.Delete(id);
        }

        [HttpDelete]
        [Route("DeleteAllFromShoppingCart")]
        public void DeleteAllFromShoppingCart()
        {
            shoppingCartService.DeleteAll();
        }

        [HttpGet]
        [Route("GetCount")]
        public int GetCount()
        {
            return shoppingCartService.GetCount();
        }
    }
}
